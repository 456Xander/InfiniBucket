package at.xander.infinibucket.recipe;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BooleanSupplier;

import com.google.gson.JsonObject;

import at.xander.infinibucket.main.InfiniBucket;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.common.crafting.conditions.IConditionSerializer;

public class ConfigCondition implements ICondition {
	private static final ResourceLocation loc = new ResourceLocation(InfiniBucket.MODID, "config_option");

	public static final BooleanSupplier TRUE = () -> true;
	public static final BooleanSupplier FALSE = () -> false;

	private static Map<String, BooleanSupplier> conditions = new HashMap<>();

	private final String option;

	public static void addConfigOption(String option, BooleanSupplier supplier) {
		conditions.put(option, supplier);
	}

	public ConfigCondition(String option) {
		this.option = option;
	}

	@Override
	public ResourceLocation getID() {
		return loc;
	}

	@Override
	public boolean test() {
		return conditions.get(option).getAsBoolean();
	}

	public static class Serializer implements IConditionSerializer<ConfigCondition> {
		private static final String optionName = "condition";

		@Override
		public void write(JsonObject json, ConfigCondition value) {
			json.addProperty(optionName, value.option);
		}

		@Override
		public ConfigCondition read(JsonObject json) {
			String option = json.get(optionName).getAsString();
			return new ConfigCondition(option);
		}

		@Override
		public ResourceLocation getID() {
			return loc;
		}

	}
}
