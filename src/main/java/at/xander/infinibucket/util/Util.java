package at.xander.infinibucket.util;

import java.util.List;
import java.util.Queue;

import com.google.common.collect.Lists;

import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.util.Direction;
import net.minecraft.util.Tuple;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class Util {

	public static boolean absorb(World worldIn, BlockPos pos) {
		Queue<Tuple<BlockPos, Integer>> queue = Lists.<Tuple<BlockPos, Integer>>newLinkedList();
		List<BlockPos> list = Lists.<BlockPos>newArrayList();
		queue.add(new Tuple<>(pos, Integer.valueOf(0)));
		int i = 0;
	
		while (!queue.isEmpty()) {
			Tuple<BlockPos, Integer> tuple = queue.poll();
			BlockPos blockpos = tuple.getA();
			int j = ((Integer) tuple.getB()).intValue();
	
			for (Direction enumfacing : Direction.values()) {
				BlockPos blockpos1 = blockpos.offset(enumfacing);
	
				if (worldIn.getBlockState(blockpos1).getMaterial() == Material.WATER) {
					worldIn.setBlockState(blockpos1, Blocks.AIR.getDefaultState(), 2);
					list.add(blockpos1);
					++i;
	
					if (j < 6) {
						queue.add(new Tuple<>(blockpos1, j + 1));
					}
				}
			}
	
			if (i > 64) {
				break;
			}
		}
	
		for (BlockPos blockpos2 : list) {
			worldIn.notifyNeighborsOfStateChange(blockpos2, Blocks.AIR);
		}
	
		return i > 0;
	}

}
