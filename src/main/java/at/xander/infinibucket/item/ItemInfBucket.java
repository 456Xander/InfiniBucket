package at.xander.infinibucket.item;

import java.util.function.IntSupplier;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CauldronBlock;
import net.minecraft.block.ILiquidContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BucketItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

public class ItemInfBucket extends BucketItem {
	private final IntSupplier capacitySupplier;

	public ItemInfBucket(IntSupplier capacitySupplier) {
		super(Fluids.WATER.delegate, new Item.Properties().maxStackSize(1).group(ItemGroup.MISC));
		this.capacitySupplier = capacitySupplier;
	}
	
	@Override
	public int getMaxDamage(ItemStack stack) {
		return capacitySupplier.getAsInt();
	}
	
	/**
	 * Called to trigger the item's "innate" right click behavior. To handle when
	 * this item is used on a Block, see {@link #onItemUse}.
	 */
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		RayTraceResult raytraceresult = rayTrace(worldIn, playerIn,
				itemstack.getDamage() != 0 ? RayTraceContext.FluidMode.SOURCE_ONLY : RayTraceContext.FluidMode.NONE);
		if (raytraceresult.getType() == RayTraceResult.Type.MISS) {
			return ActionResult.resultPass(itemstack);
		} else if (raytraceresult.getType() != RayTraceResult.Type.BLOCK) {
			return ActionResult.resultPass(itemstack);
		} else {
			BlockRayTraceResult blockraytraceresult = (BlockRayTraceResult) raytraceresult;
			BlockPos blockpos = blockraytraceresult.getPos();
			Direction direction = blockraytraceresult.getFace();
			BlockPos blockpos1 = blockpos.offset(direction);
			if (worldIn.isBlockModifiable(playerIn, blockpos)
					&& playerIn.canPlayerEdit(blockpos1, direction, itemstack)) {
				int capacity = capacitySupplier.getAsInt();
				BlockState blockstate1 = worldIn.getBlockState(blockpos);
				if (blockstate1.getMaterial() == Material.WATER) {
					worldIn.setBlockState(blockpos, Blocks.AIR.getDefaultState(), 11);
					playerIn.addStat(Stats.ITEM_USED.get(this));
					playerIn.playSound(SoundEvents.ITEM_BUCKET_FILL, 1.0F, 1.0F);
					itemstack.setDamage(0);
					return ActionResult.resultSuccess(itemstack);
				} else if (capacity != 0 && itemstack.getDamage() == capacity) {
					return ActionResult.resultPass(itemstack);
				} else if (blockstate1.getBlock() instanceof CauldronBlock) {
					// Block is Cauldron

					// System.out.println("Filling Cauldron");
					CauldronBlock cauldron = (CauldronBlock) blockstate1.getBlock();
					// int level = ((Integer) blockstate1.getValue(CauldronBlock.LEVEL)).intValue();
					int level = 0; // TODO
					if (level < 3 && !worldIn.isRemote) {
						playerIn.addStat(Stats.FILL_CAULDRON);
						cauldron.setWaterLevel(worldIn, blockpos, blockstate1, 3);

						itemstack.damageItem(1, playerIn, i -> {
						});

						playerIn.playSound(SoundEvents.ITEM_BUCKET_EMPTY, 1.0F, 1.0F);
					}
					return ActionResult.resultSuccess(itemstack);

				} else {
					BlockState blockstate = worldIn.getBlockState(blockpos);
					BlockPos blockpos2 = canBlockContainFluid(worldIn, blockpos, blockstate) ? blockpos : blockpos1;
					if (this.tryPlaceContainedLiquid(playerIn, worldIn, blockpos2, blockraytraceresult)) {
						this.onLiquidPlaced(worldIn, itemstack, blockpos2);
						if (playerIn instanceof ServerPlayerEntity) {
							CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayerEntity) playerIn, blockpos2, itemstack);
						}

						playerIn.addStat(Stats.ITEM_USED.get(this));
						return ActionResult.resultSuccess(this.emptyBucket(itemstack, playerIn));
					} else {
						return ActionResult.resultFail(itemstack);
					}
				}
			} else {
				return ActionResult.resultFail(itemstack);
			}
		}
	}

	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, CompoundNBT nbt) {
		if (this.getClass() == ItemInfBucket.class) {
			return new FluidHandlerInfiniBucket(stack, capacitySupplier.getAsInt() == 0, Fluids.WATER);
		}
		return super.initCapabilities(stack, nbt);
	}

	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return stack.getDamage() != 0;
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		int capacity = capacitySupplier.getAsInt();
		return capacity != 0 ? ((double) stack.getDamage() / capacity) : 0;
	}

	private boolean canBlockContainFluid(World worldIn, BlockPos posIn, BlockState blockstate) {
		return blockstate.getBlock() instanceof ILiquidContainer
				&& ((ILiquidContainer) blockstate.getBlock()).canContainFluid(worldIn, posIn, blockstate, Fluids.WATER);
	}

}
