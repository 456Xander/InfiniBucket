package at.xander.infinibucket.item;

import java.util.List;

import at.xander.infinibucket.block.BlockInfiniteSponge;
import at.xander.infinibucket.main.InfiniBucket;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;

public class IBItems {
	public static Item itemInfiniBucket;
	public static Item itemInfiniSpongeStick;
	public static Block blockInfiniSponge;
	public static Item itemBlockInfiniSponge;

	public static void registerItems(List<Item> registry) {
		createItems();
		registry.add(itemInfiniBucket);
		registry.add(itemBlockInfiniSponge);
		registry.add(itemInfiniSpongeStick);
	}

	public static void registerBlocks(List<Block> registry) {
		createBlocks();
		registry.add(blockInfiniSponge);
	}

	private static void createBlocks() {
		blockInfiniSponge = new BlockInfiniteSponge().setRegistryName(InfiniBucket.MODID, "infini_sponge");
	}

	public static void createItems() {
		itemInfiniBucket = new ItemInfBucket(InfiniBucket.instance.getConfig()::getCapacity)
				.setRegistryName("infini_bucket");
		itemBlockInfiniSponge = new BlockItem(blockInfiniSponge, new Item.Properties())
				.setRegistryName("infini_sponge");
		itemInfiniSpongeStick = new ItemInfSpongeStick().setRegistryName("spongestick");

	}
}
