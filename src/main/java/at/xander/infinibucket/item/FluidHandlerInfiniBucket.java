package at.xander.infinibucket.item;

import org.apache.logging.log4j.Level;

import at.xander.infinibucket.main.InfiniBucket;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;

public class FluidHandlerInfiniBucket implements IFluidHandlerItem, ICapabilityProvider {

	private ItemStack container;
	private FluidStack fluid;
	private boolean realInfinite = false;

	public FluidHandlerInfiniBucket(ItemStack container, boolean infinite, FluidStack fluid) {
		this.container = container;
		this.fluid = fluid;
		realInfinite = infinite;
	}

	public FluidHandlerInfiniBucket(ItemStack container, boolean infinite, Fluid fluid) {
		this.container = container;
		this.realInfinite = infinite;
		this.fluid = new FluidStack(fluid, container.getMaxDamage() - container.getDamage());
		if (this.fluid.getAmount() == 0) {
			this.fluid.setAmount(1);
		}
	}

	@Override
	public ItemStack getContainer() {
		return container;
	}

	@Override
	public int getTanks() {
		return 1;
	}

	@Override
	public FluidStack getFluidInTank(int tank) {
		FluidStack fl = fluid.copy();
		fl.setAmount(1000 * (container.getMaxDamage() - container.getDamage()));
		return null;
	}

	@Override
	public int getTankCapacity(int tank) {
		return 1000 * container.getMaxDamage();
	}

	@Override
	public boolean isFluidValid(int tank, FluidStack stack) {
		return stack.getFluid().isEquivalentTo(Fluids.WATER);
	}

	@Override
	public int fill(FluidStack resource, FluidAction action) {
		return 0;
	}

	@Override
	public FluidStack drain(FluidStack resource, FluidAction action) {
		if (resource == null || resource.getAmount() <= 0 || !resource.isFluidEqual(fluid)) {
			return null;
		}
		return drain(resource.getAmount(), action);
	}

	@Override
	public FluidStack drain(int maxDrain, FluidAction action) {
		if (container.getCount() != 1 || maxDrain <= 0) {
			return null;
		}
		if (fluid == null || (container.getMaxDamage() - container.getDamage()) <= 0) {
			return null;
		}
		if (InfiniBucket.DEBUG) {
			InfiniBucket.logger.log(Level.INFO, "Draining FluidStack, " + realInfinite + ", " + fluid.getAmount());
		}

		final int drainAmount = realInfinite ? maxDrain
				: Math.min((container.getMaxDamage() - container.getDamage()) * 1000, maxDrain);
		FluidStack drained = fluid.copy();
		drained.setAmount(drainAmount);
		if (action == FluidAction.EXECUTE && !realInfinite) {
			container.setDamage(container.getDamage() + drainAmount / 1000);
		}
		return drained;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		if (cap == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY) {
			// T = IFluidHandlerItem
			return (LazyOptional<T>) LazyOptional.of(() -> this);
		}
		return LazyOptional.empty();
	}

}
