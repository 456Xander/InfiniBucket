package at.xander.infinibucket.item;

import java.util.ArrayList;
import java.util.List;

import at.xander.infinibucket.main.InfiniBucket;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;

@EventBusSubscriber(modid = InfiniBucket.MODID, bus = EventBusSubscriber.Bus.MOD)
public class RegistryHandler {
	@SubscribeEvent
	public static void onItemRegistry(Register<Item> event) {
		List<Item> items = new ArrayList<>();
		IBItems.registerItems(items);
		items.forEach(i -> event.getRegistry().register(i));
	}

	@SubscribeEvent
	public static void onBlockRegistry(Register<Block> event) {
		List<Block> items = new ArrayList<>();
		IBItems.registerBlocks(items);
		items.forEach(i -> event.getRegistry().register(i));
	}
}
