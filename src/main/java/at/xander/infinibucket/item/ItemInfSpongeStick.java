package at.xander.infinibucket.item;

import at.xander.infinibucket.util.Util;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class ItemInfSpongeStick extends Item {
	public ItemInfSpongeStick() {
		super(new Item.Properties().group(ItemGroup.MISC).maxStackSize(1));
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		if (!worldIn.isRemote) {
			RayTraceResult trace = rayTrace(worldIn, playerIn, RayTraceContext.FluidMode.ANY);
			if (trace == null) {
				return ActionResult.resultPass(itemstack);
			} else if (trace.getType() != RayTraceResult.Type.BLOCK) {
				return ActionResult.resultPass(itemstack);
			}
			BlockRayTraceResult blockResult = (BlockRayTraceResult) trace;
			BlockPos blockpos = blockResult.getPos();
			if (!worldIn.isBlockModifiable(playerIn, blockpos)) {
				return ActionResult.resultFail(itemstack);
			}
			BlockState iblockstate = worldIn.getBlockState(blockpos);
			Material material = iblockstate.getMaterial();

			if (material == Material.WATER) {
				Util.absorb(worldIn, blockpos);
			}
			return ActionResult.resultSuccess(itemstack);
		}
		return ActionResult.resultPass(itemstack);
	}
}
