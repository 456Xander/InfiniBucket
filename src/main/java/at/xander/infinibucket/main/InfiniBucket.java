package at.xander.infinibucket.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.xander.infinibucket.recipe.ConfigCondition;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(InfiniBucket.MODID)
public class InfiniBucket {
	public static final boolean DEBUG = false;

	public static final String MODID = "infinibucket";

	public static InfiniBucket instance;

	public static Logger logger;

	private final Config config = new Config();

	public InfiniBucket() {
		logger = LogManager.getLogger(getClass());
		IEventBus eventBus = FMLJavaModLoadingContext.get().getModEventBus();
		eventBus.addListener(this::init);
		eventBus.addListener(this::initClient);
		ConfigCondition.addConfigOption("crafting_sponge", config::isSpongeCraftingEnabled);
		instance = this;
	}

	public Config getConfig() {
		return config;
	}

	public void init(FMLCommonSetupEvent e) {
		CraftingHelper.register(new ConfigCondition.Serializer());
	}

	public void initClient(FMLClientSetupEvent e) {
	}
}
