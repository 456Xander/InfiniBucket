package at.xander.infinibucket.main;

import org.apache.logging.log4j.LogManager;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;

@Mod.EventBusSubscriber(modid = InfiniBucket.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class Config {
	private final ForgeConfigSpec.ConfigValue<Integer> capacityConfig;
	private final ForgeConfigSpec.ConfigValue<Boolean> enableSponge;
	public final ForgeConfigSpec conf;

	public boolean isLoaded() {
		return conf.isLoaded();
	}

	public int getCapacity() {
		return capacityConfig.get();
	}
	
	public boolean isSpongeCraftingEnabled() {
		return enableSponge.get();
	}

	Config() {
		ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
		builder.push("general");
		capacityConfig = builder.comment(
				"Defines the capacity of the InfiniBucket. A value of 0 means a truly infinite water source",
				"The bucket will always be completly filled with a single water source, independent of capacity")
				.define("capacity", 0);
		enableSponge = builder.comment("Enable crafting a Sponge from wool and 4 slime balls")
				.define("enableSpongeCrafting", true);
		builder.pop();
		conf = builder.build();
	}

	@SubscribeEvent
	public static void onLoad(final ModConfig.Loading configEvent) {
		LogManager.getLogger().debug("Loaded JRFTL config file {}", configEvent.getConfig().getFileName());
	}

}
